module gitee.com/quant1x/pandas

go 1.21.5

require (
	gitee.com/quant1x/gox v1.16.3
	github.com/tealeg/xlsx/v3 v3.3.4
	gonum.org/v1/gonum v0.14.0
)

require (
	gitee.com/quant1x/pkg v0.1.8 // indirect
	github.com/frankban/quicktest v1.14.6 // indirect
	github.com/google/btree v1.1.2 // indirect
	github.com/google/go-cmp v0.6.0 // indirect
	github.com/kr/pretty v0.3.1 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/peterbourgon/diskv/v3 v3.0.1 // indirect
	github.com/rogpeppe/fastuuid v1.2.0 // indirect
	github.com/rogpeppe/go-internal v1.12.0 // indirect
	github.com/shabbyrobe/xmlwriter v0.0.0-20230525083848-85336ec334fa // indirect
	golang.org/x/exp v0.0.0-20231226003508-02704c960a9b // indirect
	golang.org/x/sys v0.15.0 // indirect
	golang.org/x/text v0.14.0 // indirect
)
